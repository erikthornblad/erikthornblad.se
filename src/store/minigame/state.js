import svenskakommuner from "assets/json/svenskalandskap.geo.json";
var allRegions = svenskakommuner.map(kommun => kommun.properties.landskap)

export default function () {
  return {
    gameState: "preGame", //preGame, inGame or postGame
    nextRegion: "",
    allRegions: allRegions,
    remainingRegions: allRegions,
    correctRegions: [],
    incorrectRegions: [],
    incorrectFeatures: [],
    highScore: 0,
    round: 0
  }
}
