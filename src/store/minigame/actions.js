import localforage from 'localforage'


///everything in this file should have been a promise
//lessons: think about game state and state machine when building this - this was quite messy, and it's a simple game

export function updateNextRegion({ commit }) {
    commit('updateNextRegion')
}

export function appendIncorrectRegion({ commit }, region) {
    commit('appendIncorrectRegion', region)
}

export function advanceGame({ commit, state, dispatch }) {
    console.log(state.gameState)

    if (state.gameState === "preGame") {
        commit('updateGameState', "inGame")
        dispatch('updateNextRegion')
    } else if (state.gameState === "inGame") {
        commit('updateGameState', "postGame")
        dispatch('setHighScore')
    } else if (state.gameState === "postGame") {
        commit('updateGameState', "preGame")
        dispatch('refreshGameStats')
        dispatch('refreshHighScore')
    }
}

export function setHighScore({ state, getters }) {
    localforage.getItem('highScore').then(function(value) {
        console.log(value)
        console.log(getters['score'])
        if (value < getters['score']){
            console.log("setting high score")
            commit('refreshHighScore', getters['score'])
        } else {
            console.log('new highscore not high enough')
        }
    }).catch(function(err) {
        localforage.setItem('highScore', getters['score'])
    }
    )
}

export function refreshHighScore({ state, commit }) {
    localforage.getItem('highScore').then(function(value) {
        commit('refreshHighScore', value)
    }).catch(function(err) {
        console.log(err)
    }
    )
}

export function appendCorrectRegion({ commit }, region) {
    commit('appendCorrectRegion', region)
}

export function appendFeature({ commit }, feature) {
    commit('appendFeature', feature)
}

export function refreshGameStats({ commit }, region) {
    commit('refreshGameStats')
}

export async function removeRegionFromAvailable({ commit }, region) {
    await commit('removeRegionFromAvailable', region)
}

export async function processGuess({ commit, state, dispatch }, region) {
    if (region === state.nextRegion) {
        dispatch('appendCorrectRegion', region)
    } else {
        dispatch('appendIncorrectRegion', region)
    }
    console.log("hello!!")
    
    await dispatch('removeRegionFromAvailable', region)

    if (state.remainingRegions.length === 0){
        dispatch('advanceGame')
    } 
    dispatch('updateNextRegion')

}
