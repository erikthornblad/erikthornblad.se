export function numberIncorrectRegions (state) {
    return state.incorrectRegions.length
}

export function numberCorrectRegions (state) {
    return state.correctRegions.length
}

export function numberGuessesMade (state) {
    return state.correctRegions.length + state.incorrectRegions.length
}

export function score (state) {
    return state.correctRegions.length
}

export function maxScorePossible (state) {
    return state.allRegions.length
}