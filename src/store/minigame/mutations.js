export function appendIncorrectRegion (state, region) {
    state.incorrectRegions.push(region)
}

export function appendCorrectRegion (state, region) {
    state.correctRegions.push(region)
}

export function updateGameState(state, newState){
    state.gameState = newState
}

export function refreshGameStats(state, newState){
    state.incorrectRegions = []
    state.correctRegions = []
    state.remainingRegions = state.allRegions
    state.round += 1 //force map to rerender
}


export function appendFeature (state, feature) {
    state.incorrectFeatures = [feature]
}

export function refreshHighScore (state, value) {
    state.highScore = value
}

export async function removeRegionFromAvailable (state, region) {
    state.remainingRegions = state.remainingRegions.filter(f => f!== region)
}

export function updateNextRegion (state) {
    state.nextRegion = state.remainingRegions[Math.floor(Math.random() * state.remainingRegions.length)];
}