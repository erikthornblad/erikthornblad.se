// This is just an example,
// so you can safely delete all default props below

export default {
  mathart: {
    title: "Mathematical art",
    subtitle: `These pictures were generated using paper.js. Click on the image to see some variants!`,
    buttons: {
      "redraw": "Resample",
      "export": "Export"
    }
  },
  map: {
    title: "Maps in OpenLayer",
    subtitle: `Below you see some different maps. 
    The data sources are all publically available, 
    and the maps are built with OpenLayer.
    `,
    tabs: {
      "minigame": "Mini Game",
      "custommap": "Customize"
    },
    minigame: {
      missionbox: {
        find: "Find",
        play: "PLAY"
      },
      messagebox: {
        instructions: "Click the region you are tasked to find!",
        numberIncorrect: "Incorrect guesses"
      }
    }
  },
    awsinfrastructure: {
      title: "How is this webpage run?",
      subtitle: "This website runs entirely in the cloud. The image below was created on app.cloudcraft.io and shows which AWS resources this webpage uses. Click the picture to enlarge it."
    },
    timeline: {
        header: "Career Timeline",
        content: [
            {
              dates: "2021-ongoing",
              title: "Senior Software Engineer Recommendations (Coupang)",
              description: `
              Coupang is South Korea's largest e-commerce player. My work is in the Recommendations Team.
                `,
              icon: "work"
            },
            {
              dates: "2020-2021",
              title: "Internal Solutions Tech Lead (Qliro)",
              description: `
              Qliro is the second largest Buy-Now-Pay-Later provider in the nordics.
              My team supported built tooling for other teams and supported in automation. Full-stack role.
                `,
              icon: "work"
            },
            {
              dates: "2019-2020",
              title: "Credit Infrastructure Manager (Qliro)",
              description: `
                In this role, I moved towards building tooling for data scientists.
                I deployed and managed Airflow, Jupyter Server, RStudio Server, Docker Swarm. I dockerised our setup
                and defined ML deployment pipelines, and ensured we were able to switch from R to Python for ML.
                `,
              icon: "work"
            },
            {
              dates: "2018-2019",
              title: "Data Scientist (Qliro)",
              description: `
                I developed credit scoring models in R, 
                and set up big data pipelines using Scala/Spark/Kafka.
                `,
              icon: "work"
            },
            {
              dates: "2018",
              title: "Data Scientist (Advantage AI)",
              description: `
                I worked as a consultant developing churn models in the mobile industry, 
                using Python and AWS Sagemaker.`,
              icon: "work",
              color: "green"
            },
            {
              dates: "2013-2018",
              title: "PhD (Uppsala University)",
              description: `
                My field of research was Probabilistic Combinatorics, 
                and my thesis was called 
                <a href="http://urn.kb.se/resolve?urn=urn:nbn:se:uu:diva-339025">Degrees in random graphs and tournament limits</a>.
                `,
              icon: "school",
              color: "#FFD700"
            },
            {
              dates: "2009-2013",
              title: "MMath (University of Oxford)",
              description:
                `Pure mathematics, in particular probability theory and topology.
                Thesis on the unknot detection algorithm.
                `,
              color: "brown",
              icon: "school"
            },
            {
              dates: "2006-2009",
              title: "International Baccalaureate (Växjö Katedralskola)",
              icon: "school",
              color: "brown",
              description: `
                International programme in English. 
                Focused on mathematics, chemistry and biology.
                `
            },
            {
              dates: "21 April 1991",
              title: "Birth (Södersjukhuset)",
              description: "I popped out on this day.",
              icon: "child_friendly",
              color: "pink"
            }
          ]
    },

    thingsilike: {
        content: {
            python: `
                I have used Python since 2009.
                The last few years I have been using it professionally, both for 
                ML and data engineering tasks, and for more traditional backend APIs.
                `,
            openlayers: `
                OpenLayers is an extremely powerful JavaScript framework to create interactive maps. The map on this page is made in OpenLayers.
                `,
            aws: `
                AWS is my go-to when it comes to cloud hosting. This website is hosted 
                through AWS amplify!
                `,
            vue: `
                Vue.js is a great javascript framework for frontend development. 
                This website is fully written in Vue.
            `,
            quasar: `
                Quasar is a framework of a framework. It provides a lot of features on top of 
                Vue. This website uses Quasar!
                `,
            airflow: `
                I have used Airflow since 2019. I am experienced in managing 
                Airflow instances, developing custom plugins, and developing workflows.
                `,
            docker: `
                I use Docker and docker-compose for many projects.
                Once you are familiar with it, it will remove nearly all annoying things
                when it comes to environment setup.
            `,
            paperjs: `
                Paper.js is a cool library to render graphics in the browser. The images in the art-section 
                where created in Paper.js.
            `
        }
    },

    randomwidgets: {
        title: "Random widgets",
        subtitle: "Here are some random simple widgets I built to learn some front-end development and to try out various AWS services.",
        translate: {
            tabTitle: "Translate",
            textToTranslate: `Type anything in any language in this box, and it will translate it to the chosen language. You can also listen to synthezised audio playback. The translation comes from AWS Translate, and the synthetized audio from AWS Polly.`,
            buttonLabel: "Translate",
            translateToLabel: "Target language"
        },
        howmuchwouldisave: {
            tabTitle: "Invest now",
            1: "If you start with $",
            2: "and invest $",
            3: "each month for",
            4: "years, at a yearly interest rate of",
            5: "percent, then you will end up with",
            6: ".",
        },
        weather: {
            tabTitle: "Pollution",
            temperatureNowText: "The temperature in Stockholm is:",
            temperatureTomorrowText: "Tomorrow's temperature will be",
            pollutantHeader: "Pollution",
            concentrationHeader: "Concentration",
            pollutants: {
                "co": "Carbon Monoxide",
                "no": "Nitrogen Monoxide",
                "no2": "Nitrogen Dioxide",
                "o3": "Ozone"
            },
            informationalText: "I fetched this information by making an API call to",
        }
    }
  }
  