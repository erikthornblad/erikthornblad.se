import enUS from './en-us'
import sv from './sv'

export default {
  'en-us': enUS,
  'sv': sv
}
