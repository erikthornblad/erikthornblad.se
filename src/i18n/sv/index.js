// This is just an example,
// so you can safely delete all default props below

export default {
  mathart: {
    title: "Matematisk konst",
    subtitle: `De här bilderna genererades med hjälp av paper.js. Klicka på bilden för att se lite varianter!
    `,
    buttons: {
      "redraw": "Sampla",
      "export": "Exportera"
    }
  },
  map: {
    title: "Kartor",
    subtitle: `Här finns några olika kartor jag har skapat. 
    De är byggda i OpenLayer.
    `,
    tabs: {
      "minigame": "Spela",
      "custommap": "Modifiera"
    },
    minigame: {
      missionbox: {
        find: "Hitta",
        play: "SPELA"
      },
      messagebox: {
        instructions: "Klicka på landskapet du ska hitta!",
        numberIncorrect: "Antal felaktiga svar"
      }
    }
  },
  awsinfrastructure: {
    title: "Hur körs denna hemsidan?",
    subtitle: "Allting du ser körs i molnet. Bilden nedan skapades på app.cloudcraft.io och visar de AWS-resurser som används. Klicka på bilden för att förstora den."
  },
    timeline: {
        header: "Karriär",
        content: [
            {
              dates: "2021-",
              title: "Senior Software Engineer Recommendations (Coupang)",
              description: `
              Coupang är Sydkoreas störesta e-handlare. Mitt arbete berör främst rekommendationsmotorn.
                `,
              icon: "work"
            },
            {
              dates: "2020-2021",
              title: "Internal Solutions Tech Lead (Qliro)",
              description: `
                Qliro är Nordens näst största Buy-Now-Pay-Later-provider. Mitt team stödde andra team i 
                automation och tooling.
                `,
              icon: "work"
            },
            {
              dates: "2019-2020",
              title: "Credit Infrastructure Manager (Qliro)",
              description: `
                I denna rollen gick jag mer mot att bygga redskap för data scientists. 
                Jag drev ett projekt för att sjösätta Airflow, Jupyter, RStudio Server och Docker Swarm.
                Jag definierade och dockeriserade olika ML-pipelines, och såg till att vi kunde byta från R till Python vad gäller ML.
                `,
              icon: "work"
            },
            {
              dates: "2018",
              title: "Data Scientist (Qliro)",
              description: `
                Jag utvecklade krediscoringmodeller i R och satte upp stordatapipelines i Scala/Spark/Kafka.
                `,
              icon: "work"
            },
            {
              dates: "2018-2018",
              title: "Data Scientist (Advantage AI)",
              description: `
                Jag arbetade som konsult med churnmodeller i mobilindustrin. Vi använde
                Python och AWS Sagemaker.`,
              icon: "work",
              color: "green"
            },
            {
              dates: "2013-2018",
              title: "PhD (Uppsala University)",
              description: `
                Mitt forskningsområde var diskret sannnolikhetsteori, och min avhandling hette
                <a href="http://urn.kb.se/resolve?urn=urn:nbn:se:uu:diva-339025">Degrees in random graphs and tournament limits</a>.
                `,
              icon: "school",
              color: "#FFD700"
            },
            {
              dates: "2009-2013",
              title: "MMath (University of Oxford)",
              description:
                `Jag studerade ren matematik, med fokus mot sannolikhetsteori och topologi.
                Min masteruppsats var i knutteori.
                `,
              color: "brown",
              icon: "school"
            },
            {
              dates: "2006-2009",
              title: "International Baccalaureate (Växjö Katedralskola)",
              icon: "school",
              color: "brown",
              description: `
                Internationellt program på engelska. 
                Fokus på matematik, kemi och fysik.
                `
            },
            {
              dates: "21 April 1991",
              title: "Födsel (Södersjukhuset)",
              description: "Denna dagen föddes jag!",
              icon: "child_friendly",
              color: "pink"
            }
          ]
    },

    thingsilike: {
        content: {
            python: `
                Jag har använt Python sedan 2009. De senaste åren främst för ML och data engineering, 
                samt för mer traditionella backend-APIer och hemsidor via Django och Flask.
                `,
            openlayers: `
                OpenLayers är ett kraftfull javascriptramverk för att skapa interaktiva kartor. Kartan på denna sidan är skriven i OpenLayers.
                `,
            aws: `
                På AWS hostar jag och använder deras tjänster. Denna hemsidan 
                är sköts via AWS Amplify.
                `,
            vue: `
                Vue.js is a javascriptramverk för frontendutveckling. Denna hemsidan är skriven i Vue.
            `,
            quasar: `
                Quasar är ett ramverk på ett ramverk. Denna hemsidan är skriven med hjälp av Quasar.
                `,
            airflow: `
                Jag har använt Airflow sedan 2019, och har erfarenhet i att sätta upp 
                Airflow från början, samt utveckla kundspecifika plugins och flöden.
                `,
            docker: `
                Docker och docker-compose använder jag för de flesta projekt. 
            `,
            paperjs: `
              Paper.js är ett javascriptbibliotek för programmatisk bildhantering i webbläsaren. 
              Bilderna ovan skapades med hjälp av paper.js!
              `
        }
    },

    randomwidgets: {
        title: "Snuttar",
        subtitle: "Här hittar du mindre snuttar jag skapat för att testa olika frontendsaker, samt för att testa AWS tjänster.",
        translate: {
            textToTranslate: `Skriv vad som helst i rutan och tryck på knappen. Nedan får du dels en översättning till valt språk, samt en (syntetiserad) ljudfil. Textöversättningen kommer från AWS Translate och ljudfilen från AWS Polly.`,
            tabTitle: "Översätt",
            buttonLabel: "Översätt",
            translateToLabel: "Översätt till"
        },
        howmuchwouldisave: {
            tabTitle: "Investera nu",
            1: "Om du just nu har",
            2: "kr och investerar ",
            3: "kr varje månad i",
            4: "år, och årsräntan är",
            5: "procent, så kommer du till slut ha",
            6: "kr.",
        },
        weather: {
            tabTitle: "Väder",
            temperatureNowText: "Temperaturen i Stockholm är:",
            temperatureTomorrowText: "Imorgon blir temperaturen",
            pollutantHeader: "Ämne",
            concentrationHeader: "Concentration",
            pollutants: {
                "co": "Kolmonoxid",
                "no": "Kvävemonoxid",
                "no2": "Kvävedioxid",
                "o3": "Ozon"
            },
            informationalText: "Informationen hämtades via API-anrop till",
        }
    }
  }
  