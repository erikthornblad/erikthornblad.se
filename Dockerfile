# develop stage
FROM node:12.20-alpine as develop-stage
WORKDIR /app
COPY package*.json ./
RUN npm install -g @quasar/cli
RUN npm install

EXPOSE 8080